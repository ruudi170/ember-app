/*jshint node:true*/
module.exports = function(app) {
	var express = require('express');
	var postsRouter = express.Router();

	//title: DS.attr('string'),
	//body: DS.attr('string'),
	//author: DS.attr('string')
	postsRouter.get('/', function(req, res) {
		res.send({
			data: [{
				type: "posts",
				id: "1",
				attributes: {
					id: 1, 
					title: 'example title', 
					body: 'example body', 
					author: 'example author'
				}
			}]
		});
	});

	postsRouter.post('/', function(req, res) {
		res.status(201).end();
	});

	postsRouter.get('/:id', function(req, res) {
		res.send({
			'posts': {
				id: req.params.id
			}
		});
	});

	postsRouter.put('/:id', function(req, res) {
		res.send({
			'posts': {
				id: req.params.id
			}
		});
	});

	postsRouter.delete('/:id', function(req, res) {
		res.status(204).end();
	});

	// The POST and PUT call will not contain a request body
	// because the body-parser is not included by default.
	// To use req.body, run:

	//    npm install --save-dev body-parser

	// After installing, you need to `use` the body-parser for
	// this mock uncommenting the following line:
	//
	//app.use('/api/posts', require('body-parser').json());
	app.use('/api/posts', postsRouter);
};
