var http = require('http');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var JSONAPISerializer = require('jsonapi-serializer').Serializer;

var app = express();
var port = '2002';

var server = http.Server(app);

//app.use(express.static(path.join(__dirname, 'public')));
//require('./app/routes')(app);

app.use(bodyParser.json());

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
	next();
});

app.use('/', express.static(path.join(__dirname, '../bower_components/ace-builds/src-noconflict/')));

/*
app.get('/', function(req, res){
	res.send('is working');
});
*/

app.use('/api/posts', require('./routes/posts.js'));
app.use('/api/codes', require('./routes/codes.js'));
app.use('/api/chat', require('./routes/chat.js')(server));

server.listen(port, function(){
	console.log(`http://0.0.0.0:${port}`);
});
