var path = require('path');
var express = require('express');
var app = express();

var port = 2005;

app.use(express.static(path.join(__dirname, 'public/')));

app.get('*', function(req, res){
	res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.get('/test', function(req, res){
	res.send('is working');
});

app.listen(port, function(){
	console.log(`http://0.0.0.0:${port}`);
});
