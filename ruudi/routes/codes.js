var path = require('path');
var express = require('express');
var router = express.Router();
var mongoose = require(path.join(__dirname, '../config/db.js'));

var CodeSchema = mongoose.Schema({
	title: 'string',
	body: 'string'
});
var CodeModel = mongoose.model('code', CodeSchema);

router.get('/create', function(req, res){
	var code = new CodeModel({
		title: 'example code title',
		body: 'example code body',
	});
	code.save(function(err, code){
		if (err) {
			res.json(err);
		} else {
			res.json(code);
		}
	});
});

router.get('/', function(req, res){
	CodeModel.find({}, function(err, data){
		if (err) {
			res.json({error: err});
		} else {
			res.json({codes: data});
		}
	});
});
router.get('/:id', function(req, res){
	console.log(req.params);
	CodeModel.findById(req.params.id, function(err, data){
		if (err) {
			res.json({error: err});
		} else {
			res.json({code: data});
			console.log(data);
		}
	});
});
router.delete('/:id', function(req, res){
	console.log(req.params);
	CodeModel.findByIdAndRemove(req.params.id, function(err, code){
		if (err) {
			console.log(err);
			res.json({error: err});
		} else {
			console.log(code);
			res.json({});
		}
	});
});
router.post('/', function(req, res){
	var code = new CodeModel(req.body.code);
	code.save(function(err, data){
		if (err) {
			res.json({error: err});
		} else {
			res.json({code: data});
			console.log({code: data});
		}
	});
});


module.exports = router;
