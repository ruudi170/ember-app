var path = require('path');
var express = require('express');
var router = express.Router();
var mongoose = require(path.join(__dirname, '../config/db.js'));

var PostSchema = mongoose.Schema({
	title: 'string',
	author: 'string',
	body: 'string'
});
var PostModel = mongoose.model('post', PostSchema);

/*
var PostSerializer = new JSONAPISerializer('posts', {
	attributes: ['title', 'author', 'body']
});
*/

router.get('/create', function(req, res){
	var post = new PostModel({
		title: 'example title',
		author: 'example author',
		body: 'example body',
	});
	post.save(function(err, post){
		if (err) {
			res.json(err);
		} else {
			res.json(post);
		}
	});
});

router.get('/', function(req, res){
	PostModel.find({}, function(err, data){
		if (err) {
			res.json({error: err});
		} else {
			//console.log(data);
			//var posts = PostSerializer.serialize(data);
			res.json({posts: data});
			console.log({post: data});
		}
	});
});
router.get('/:id', function(req, res){
	console.log(req.params);
	PostModel.findById(req.params.id, function(err, data){
		if (err) {
			res.json({error: err});
		} else {
			res.json({post: data});
			console.log(data);
		}
	});
	//PostModel.remove({id: req.params.id}, function(err, data){
		//if (err) {
			//res.json({error: err});
		//} else {
			//res.json({post: data});
		//}
	//});
});
router.delete('/:id', function(req, res){
	console.log(req.params);
	PostModel.findByIdAndRemove(req.params.id, function(err, post){
		if (err) {
			console.log(err);
			res.json({error: err});
		} else {
			console.log(post);
			res.json({});
		}
	});
});
router.post('/', function(req, res){
	var post = new PostModel(req.body.post);
	post.save(function(err, data){
		if (err) {
			res.json({error: err});
		} else {
			res.json({post: data});
			console.log({post: data});
		}
	});
	/*
	PostModel.find({}, function(err, data){
		if (err) {
			res.json(err);
		} else {
			var posts = PostSerializer.serialize(data);
			res.json(posts);
		}
	});
	*/
});


module.exports = router;
