
module.exports = function(server){

	var path = require('path');
	var mongoose = require(path.join(__dirname, '../config/db.js'));
	var io = require('socket.io')(server);
	var path = require('path');
	var express = require('express');
	var router = express.Router();

	/* message schema / model */
	var MessageSchema = mongoose.Schema({
		name: 'string',
		message: 'string',
	});
	var MessageModel = mongoose.model('chat/message', MessageSchema);

	/* test route */
	router.get('/test', function(req, res){
		var data = {};
		var message = new MessageModel({
			name: 'example name',
			message: 'example message',
		});
		message.save(function(err, message){
			if (err) {
				res.json(err);
			} else {
				data.createdMessage = message;
				MessageModel.find({}, function(err, messages){
					if (err) {
						res.json({error: err});
					} else {
						data.messages = messages;
						res.json(data);
					}
				});
			}
		});
		//res.send('test is working');
	});

	/* index route */
	router.get('/', function(req, res){
		res.send('is working');
	});

	/* messages route */
	router.get('/messages', function(req, res){
		MessageModel.find({}, function(err, data){
			if (err) {
				res.json({error: err});
			} else {
				//console.log(data);
				//var posts = PostSerializer.serialize(data);
				res.json({'chat/messages': data});
				//console.log({message: data});
			}
		});
	});

	/* socket io setup */
	io.on('connection', function(socket){
		console.log('a user connected');
		socket.on('chat message', function(message){
			io.emit('chat message', message);
		});
	});

	return router;
};
