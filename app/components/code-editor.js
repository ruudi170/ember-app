import Ember from 'ember';

var editor = null;

export default Ember.Component.extend({
	store: Ember.inject.service('store'),

	becomeFocused: function(){
		console.log('focused');
	},
	init() {
		this._super();
	},
	didInsertElement: function() {
		Ember.run.scheduleOnce('afterRender', this, function(){
			editor = ace.edit(this.$('#editor')[0]);
			editor.getSession().setMode('ace/mode/javascript');
			editor.focus();
		});
	},
	actions: {
		save: function() {
			var code = {
				title: 'example title',
				body: editor.getValue(),
			};
			console.log(code.body);
			console.log(this.store.createRecord('code', code));
			//const newCode = this.store.createRecord('code', code);
			//newCode.save();
			console.log('saved');
		}
	}
});
