import Ember from 'ember';

var socket = null;

export default Ember.Component.extend({
	message: '',
	init() {
		this._super();
		this._super(...arguments);
		console.log('chat-app component initialized');
	},
	didInsertElement: function() {
		Ember.run.scheduleOnce('afterRender', this, function(){
			var _this = this;
			$('#chat .send').keypress(function(event){
				if (event.which == 13) {
					_this.sendAction('save');
				}
			});
			console.log('example');
			socket = io('http://kodu.noip.me:2002');
			socket.on('chat message', function(message){
				console.log('from server: ' + message);
			});
		});
	},
	didUpdateAttrs() {
		this._super(...arguments);
		console.log('did update attributes');
	},
	didRecieveAttrs() {
		this._super(...arguments);
		console.log('recieved attributes');
	},
	actions: {
		send: function(){
			console.log($("#chat"));
			console.log('message');
			socket.emit('chat message', this.get('message'));
			this.set('message', '');
		}
	}
});
