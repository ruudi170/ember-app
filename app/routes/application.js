import Ember from 'ember';

export default Ember.Route.extend({
	loadPlugin: function() {
		// Use run loop if you need to setup the DOM first
		Ember.run.scheduleOnce('afterRender', this, function() {
			Ember.$.getScript('http://kodu.noip.me:2002/socket.io/socket.io.js');
		});
	}.on('init'),
	model: function() {
		return this.get('store').findAll('chat/message');
	}
});
