import Ember from 'ember';

export default Ember.Route.extend({
	init(){
		console.log('admin chat messages route');
		this.store.findAll('post').then(function(post){
			console.log(post.get('message'));
		});
	},
	model: function() {
		//return this.get('store').findAll('chat/message');
		return this.get('store').findAll('chat/message');
	},
});
