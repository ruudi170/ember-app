import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('about');
  this.route('admin', function() {
    this.route('posts');
    this.route('chat-messages');
  });
  this.route('editor');
  this.route('login');
  this.route('register');
  this.route('chat');
});

export default Router;
