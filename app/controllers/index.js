import Ember from 'ember';

export default Ember.Controller.extend({
	init() {
		console.log('index controller loaded');
	},
	title: '',
	body: '',
	actions: {
		save: function() {
			const author = 'ruudi konrad';
			const title = this.get('title');
			const body = this.get('body');

			var post = {
				title: title,
				body: body,
				author: author
			};
			console.log(post);

			const newPost = this.store.createRecord('post', post);
			newPost.save();

			console.log('post saved');
		},
		delete(post) {
			post.destroyRecord();
		}
		/*
		delete: function(id) {
			this.store.find('post', id).then(function(post){
				post.destroyRecord();
				this.store.commit();
			});
		}
		*/
	}
});
