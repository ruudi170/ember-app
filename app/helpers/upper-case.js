import Ember from 'ember';

//export function upperCase(params/*, hash*/) {
  //return params;
//}

export function upperCase([value]) {
	if (value) {
		return value.toUpperCase();
	} else {
		return 'value is undefined, cant uppercase';
	}
}

export default Ember.Helper.helper(upperCase);
