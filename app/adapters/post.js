/*
import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
	//namespace: '/',
	host: 'http://kodu.noip.me:2002'
});
*/
import DS from 'ember-data';
export default DS.RESTAdapter.extend({
	namespace: 'api',
	host: 'http://kodu.noip.me:2002'
});
